package ProblemSet04.Problem03;

import java.util.Arrays;
import java.util.Scanner;

abstract class Geometry {
	public abstract int getPerimeter();
	public abstract int getArea();
	public abstract String toString();
}

class Rectangle extends Geometry {
	private int width;
	private int length;
	
	public Rectangle(int width, int length) {
		this.width = width;
		this.length = length;
	}
	
	public int getPerimeter() {
		return (width + length) * 2;
	}
	
	public int getArea() {
		return width * length;
	}

	public String toString() {
		return "Rectangle [width=" + width + ", length=" + length + "]";
	}
}

class Circle extends Geometry {
	private int radius;
	
	public Circle(int radius) {
		this.radius = radius;
	}
	
	public int getPerimeter() {
		return (int) (2 * radius * Math.PI);
	}
	
	public int getArea() {
		return (int) (Math.PI * Math.pow(radius, 2));
	}

	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
}


public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Rectangle[] rect = new Rectangle[2];
		Circle[] circle = new Circle[2];
		getInput(scan, rect);
		getInput(scan, circle);
		scan.close();
		
		System.out.println(getPerimterSum(rect) + getPerimterSum(circle));
		System.out.println(getAreaSum(rect) + getAreaSum(circle));
		
		System.out.println(Arrays.deepToString(rect));
		System.out.println(Arrays.deepToString(circle));
		
		
	}
	
	public static void getInput(Scanner scan, Geometry[] arr) {
		int length = arr.length;
		for (int i = 0; i < length; i++) {
			String[] str = scan.nextLine().split(" ");
			switch (str.length) {
			case 1:
				arr[i] = new Circle(Integer.parseInt(str[0]));
				break;
			case 2:
				arr[i] = new Rectangle(Integer.parseInt(str[0]), Integer.parseInt(str[1]));
				break;
			default:
				System.out.println("Error!");
			}
		}
	}
	
	public static int getPerimterSum(Geometry[] arr) {
		int sum = 0, len = arr.length;
		for(int i = 0; i < len; i++) sum += arr[i].getPerimeter();
		
		return sum;
	}
	
	public static int getAreaSum(Geometry[] arr) {
		int sum = 0, len = arr.length;
		for(int i = 0; i < len; i++) sum += arr[i].getArea();
		
		return sum;
	}
}
