package ProblemSet04.Problem01;

import java.util.Scanner;
class Person {
	private String name;
	private int age;
	private boolean gender;
	private int id;
	
	Person() {
		System.out.println("This is constructor");
		System.out.println(name + "," + age + "," + gender + "," + id);
	}
	
	Person(String name, int age, boolean gender){
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
	
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender + ", id=" + id + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		Person[] person_list = new Person[n];
		
		for(int i = 0; i < n; i++) {
			String name = scan.next();
			int age = scan.nextInt();
			boolean gender = scan.nextBoolean();
			
			person_list[i] = new Person(name, age, gender);
		}
		scan.close();
		
		for(int i = n - 1; i >= 0; i--) System.out.println(person_list[i]);
		System.out.println(new Person());
	}
}
