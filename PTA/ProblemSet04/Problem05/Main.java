package ProblemSet04.Problem05;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

abstract class Person {
	String name;
	int age;
	boolean gender;
	
	public Person(String name, int age, boolean gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
	
	public String toString() {
		return this.name + "-" + this.age + "-" + this.gender;
	}
	
	public boolean equals(Object obj) {
		Person p = (Person) obj;
		if((p.name == this.name || p.name != null && p.name.equals(this.name)) && p.age == this.age && p.gender == this.gender)
			return true;
		
		return false;
	}
}

class Student extends Person {
	String stuNo;
	String clazz;
	
	public Student(String name, int age, boolean gender, String stuNo, String clazz) {
		super(name, age, gender);
		
		this.stuNo = stuNo;
		this.clazz = clazz;
	}
	
	public String toString() {
		return super.toString() + "-" + this.stuNo + "-" + this.clazz;
	}
	
	public boolean equals(Object obj) {
		if(super.equals(obj)) {
			Student s = (Student) obj;
			if((s.stuNo == this.stuNo || s.stuNo != null && s.stuNo.equals(this.stuNo))
					&& (s.clazz == this.clazz || s.clazz != null && s.clazz.equals(this.clazz)))
				return true;
		}
		
		return false;
	}
}

class Company{
	String name;
	
	public Company(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}
	
	public boolean equals(Object obj) {
		Company c = (Company) obj;
		if(c.name == this.name || c.name != null && c.name.equals(this.name))
			return true;
		
		return false;
	}
}

class Employee extends Person {
	Company company;
	double salary;
	
	public Employee(String name, int age, boolean gender, double salary, Company company) {
		super(name, age, gender);
		
		this.company = company;
		this.salary = salary;
	}
	
	public String toString() {
		return super.toString() + "-" + this.company + "-" + this.salary;
	}
	
	public boolean equals(Object obj) {
		if(super.equals(obj)) {
			Employee e = (Employee) obj;
			DecimalFormat df = new DecimalFormat("#.#");
			if((e.company == this.company || e.company != null && e.company.equals(this.company)) && df.format(e.salary).equals(df.format(this.salary)))
				return true;
		}
		
		return false;
	}
}

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		List<Person> personList = new ArrayList<Person>();
		boolean continueCreate = true;
		
		String str = null;
		while(true) {
			str = scan.nextLine();
			if(!"exit".equals(str)) {
				if(continueCreate) {
					String[] cmd = split(str, " ");
					switch(cmd[0]) {
						case "s":
							personList.add(new Student(cmd[1], Integer.parseInt(cmd[2]), Boolean.parseBoolean(cmd[3]), cmd[4], cmd[5]));
							break;
						case "e":
							personList.add(new Employee(cmd[1], Integer.parseInt(cmd[2]), Boolean.parseBoolean(cmd[3]), Double.parseDouble(cmd[4]), cmd[5] == null ? null : new Company(cmd[5])));
							break;
						default:
							//结束创建
							continueCreate = false;
							//开始排序
							personList.sort(new Comparator<Person>() {
								public int compare(Person p1, Person p2) {
									int sort = p1.name.compareTo(p2.name);
									if(sort == 0)
										return p1.age - p2.age;
									
									return sort;
								}
							});
							
							//输出排序结果
							int len = personList.size();
							for(int i = 0; i < len; i++) {
								Person p = personList.get(i);
								System.out.println(p instanceof Student ? "Student:" + (Student) p : "Employee:" + (Employee) p);
							}
					}
				} else {
					//对personList进行分隔
					ArrayList<Student> stuList = new ArrayList<Student>();
					ArrayList<Employee> empList = new ArrayList<Employee>();
					int len = personList.size();
					
					for(int i = 0; i < len; i++) {
						Person p = personList.get(i);
						if(p instanceof Student) {
							if(stuList.indexOf(p) == -1) stuList.add((Student) p);
						} else {
							if(empList.indexOf(p) == -1) empList.add((Employee) p);
						}
					}
					
					//输出
					printPersonList(stuList);
					printPersonList(empList);
				}
			} else break;
		}
		
		scan.close();
	}
	
	// 分割字符串，并将null字符串转为真正的null
	public static String[] split(String str, String regex) {
		String[] init = str.split(regex);
		int len = init.length;
		for(int i = 0; i < len; i++) {
			if("null".equals(init[i])) init[i] = null;
		}
		
		return init;
	}
	
	// 根据对象类型输出列表
	public static <T> void printPersonList(ArrayList<T> list) {
		int len = list.size();
		if(len > 0) {
			boolean is_stu = list.get(0) instanceof Student;
			System.out.println(is_stu ? "stuList" : "empList");
			for(int i = 0; i < len; i++) {
				if(is_stu)
					System.out.println("Student:" + ((Student) list.get(i)).toString());
				else
					System.out.println("Employee:" + ((Employee) list.get(i)).toString());
			}
		}
		
	}
}
