package ProblemSet04.Problem04;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

class PersonOverride {
	private String name;
	private int age;
	private boolean gender;
	
	public PersonOverride() {
		this("default", 1, true);
	}
	
	public PersonOverride(String name, int age, boolean gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
	
	public String toString() {
		return this.name + "-" + age + "-" + gender;
	}
	
	public boolean equals(Object obj) {
		PersonOverride po = (PersonOverride) obj;
		if((po.name == this.name || po.name != null && po.name.equals(this.name)) && po.age == this.age && po.gender == this.gender)
			return true;
		
		return false;
	}
}

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		int n1 = scan.nextInt();
		ArrayList<PersonOverride> persons1 = new ArrayList<PersonOverride>();
		for(int i = 0; i < n1; i++) persons1.add(new PersonOverride());
		
		int n2 = scan.nextInt();
		ArrayList<PersonOverride> persons2 = new ArrayList<PersonOverride>();
		for(int i = 0; i < n2; i++) {
			String name = scan.next();
			int age = scan.nextInt();
			boolean gender = scan.nextBoolean();
			
			PersonOverride person = new PersonOverride(name, age, gender);
			if(persons2.indexOf(person) == -1) persons2.add(person);
		}
		scan.close();
		
		
		for(int i = 0; i < n1; i++) 
			System.out.println(persons1.get(i));
		
		n2 = persons2.size();
		for(int i = 0; i < n2; i++) 
			System.out.println(persons2.get(i));
		
		System.out.println(n2);
		System.out.println(Arrays.toString(PersonOverride.class.getConstructors()));
	}
}
