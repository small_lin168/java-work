package ProblemSet02.Problem02;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		while(scan.hasNextLine()) {
			int a = scan.nextInt(),
				b = scan.nextInt();
			
			System.out.println(Math.abs(a) > 1000 ? "|a|>1000" : a+b);
		}
		
		scan.close();
	}
}
