package ProblemSet02.Problem05;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		while(scan.hasNextDouble()) {
			double num = scan.nextDouble(), factor = 0, result = 0;
			
			if(num < 0) System.out.println(Double.NaN);
			else if(num == 0) System.out.printf("%.6f\n", factor);
			else {
				while(result < num && Math.abs(num - result) > 0.0001) {
					result = Math.pow(factor, 2);
					//factor = new BigDecimal(Double.toString(factor)).add(new BigDecimal("0.0001")).doubleValue();
					factor += 0.0001;
				}
				
				//System.out.printf("%.6f\n", new BigDecimal(Double.toString(factor)).subtract(new BigDecimal("0.0001")).doubleValue());
				System.out.printf("%.6f\n", factor - 0.0001);
			}
		}
		
		scan.close();
	}
}
