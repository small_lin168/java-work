package ProblemSet02.Problem04;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		while(scan.hasNextLine()){
			String num_str = String.valueOf(scan.nextFloat());
			int str_len = num_str.length(), sum = 0;
			
			for(int i=0; i<str_len; i++){
				char current_item = num_str.charAt(i);
				if(current_item >= '0' && current_item <= '9'){
					sum += Integer.valueOf(String.valueOf(current_item));
				}
			}
			
			System.out.println(sum);
		}
		
		scan.close();
	}
}
