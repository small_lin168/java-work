package ProblemSet02.Problem01;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		while(scan.hasNext()) {
			int f = scan.nextInt();
			if (f <= 0) break;
			
			for(int i=0; i<f; i++) {
				for(int j=0; j<f+i; j++) {
					System.out.print(j >= f-i-1 ? '*' : ' ');
				}
				System.out.println();
			}
		}
		
		scan.close();
	}
}
