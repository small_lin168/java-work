package ProblemSet02.Problem03;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		while(scan.hasNextInt()){
			int num = scan.nextInt();
			
			if(num >= 10000 && num <= 20000){
				System.out.println(Integer.toBinaryString(num) + ',' + Integer.toOctalString(num) + ',' + Integer.toHexString(num));
			} else {
//				int sum = 0, index = 0;
//				String num2str = Integer.toString(num >= 0 ? num : Math.abs(num));
//				while(index < num2str.length()){
//					int tmp = Integer.parseInt(String.valueOf(num2str.charAt(index++)));
//					System.out.print(tmp + " ");
//					
//					sum += tmp;
//				}
//				
//				System.out.println(sum);
				
				if(num < 0) num = Math.abs(num);
				int num_len = Integer.toString(num).length(), sum = 0;
				while(num_len > 0){
					int pow_num = (int)Math.pow(10, --num_len), tmp = num / pow_num;
					System.out.print(tmp + " ");
					
					num %= pow_num;
					sum += tmp;
				}
				System.out.println(sum);
			}
		}
		
		scan.close();
	}
}
