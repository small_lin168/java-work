package ProblemSet03.Problem04;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Comparator;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int id_len = Integer.parseInt(scan.nextLine());
		String[] id_list = new String[id_len];
		for (int i = 0; i < id_len; i++) id_list[i] = scan.nextLine();
		Arrays.sort(id_list, new Comparator<String>() {
			public int compare(String o1, String o2) {
				int o1_y = Integer.parseInt(o1.substring(6, 10)),
					o1_m = Integer.parseInt(o1.substring(10, 12)),
					o1_d = Integer.parseInt(o1.substring(12, 14)),
					o2_y = Integer.parseInt(o2.substring(6, 10)),
					o2_m = Integer.parseInt(o2.substring(10, 12)),
					o2_d = Integer.parseInt(o2.substring(12, 14));
				
				if(o1_y == o2_y) {
					if(o1_m == o2_m) {
						if(o1_d == o2_d) return 0;
						
						return o1_d - o2_d;
					}
					
					return o1_m - o2_m;
				}
				
				return o1_y - o2_y;
			}
		});
		
		boolean continue_listen = true;
		while(continue_listen) {
			String cmd = scan.nextLine();
			switch(cmd) {
				case "sort1":
					for(int i = 0; i < id_len; i++) {
						System.out.println(id_list[i].substring(6,10) + "-" + id_list[i].substring(10,12) + "-" + id_list[i].substring(12,14));
					}
					break;
				case "sort2":
					for(int i = 0; i < id_len; i++) {
						System.out.println(id_list[i]);
					}
					break;
				default:
					continue_listen = false;
			}
		}
		
		scan.close();
		System.out.println("exit");
	}
}
