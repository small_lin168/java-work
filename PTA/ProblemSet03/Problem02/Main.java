package ProblemSet03.Problem02;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		boolean continueListen = true;
		String method = null;
		int[] num_arr = null;
		while(continueListen) {
			method = scan.nextLine();
			switch(method) {
				case "fib":
					int length = Integer.parseInt(scan.nextLine());
					for(int i = 1; i <= length; i++) {
						System.out.print(fibonacci(i) + (i == length ? "" : " "));
					}
					System.out.println();
					break;
				case "sort":
					num_arr = Arrays.asList(scan.nextLine().split(" ")).stream().mapToInt(Integer::parseInt).toArray();
					Arrays.sort(num_arr);
					System.out.println(Arrays.toString(num_arr));
					break;
				case "search":
					int index = Arrays.binarySearch(num_arr, Integer.parseInt(scan.nextLine()));
					System.out.println(index < 0 ? -1 : index);
					break;
				case "getBirthDate":
					int count = Integer.parseInt(scan.nextLine());
					StringBuilder birthdayList = new StringBuilder();
					for(int i = 0; i < count; i++) {
						String id = scan.nextLine();
						birthdayList.append(id.substring(6, 10) + "-" +  id.substring(10, 12) + "-" +  id.substring(12, 14) + "\n");
					}
					System.out.print(birthdayList);
					
					break;
				default:
					continueListen = false;
			}
		}
		scan.close();
		System.out.println("exit");
	}
	
	public static long fibonacci(long number) {
		if((number == 0) || (number == 1))
			return number;
		
		return fibonacci(number - 1) + fibonacci(number - 2);
	}
}
