package ProblemSet03.Problem03;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		int n, begin, end;
		while(true) {
			n = scan.nextInt();
			begin = scan.nextInt();
			end = scan.nextInt();
			StringBuilder str = new StringBuilder();
			
			for(int i=0; i<n; i++) str.append(i);
			System.out.println(str.substring(begin, end));
		}
	}
}
