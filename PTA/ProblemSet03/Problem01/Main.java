package ProblemSet03.Problem01;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		while(true) {
			int score = 0, min = 0, max = 0;
			for(int i = 0; i < 10; i++) {
				int tmp = scan.nextInt();
				score += tmp;
				if(i == 0 || min > tmp) min = tmp;
				if(max < tmp) max = tmp;
			}
			
			scan.close();
			
			System.out.println(max + " " + min);
			System.out.println((score - max - min) / 8);
		}
	}
}
