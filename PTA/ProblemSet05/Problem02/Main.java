package ProblemSet05.Problem02;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String str = scan.nextLine();
		scan.close();
		
		System.out.println(isPalindrome(str, str.length(), 0) ? "yes" : "no");
	}
	
	public static boolean isPalindrome(String str, int length, int offset) {
		if (length < 2) return false;
		
		int middleIndex = length / 2,
			fixOffset = (length % 2 == 0) ? 0 : 1;
		//fixOffset用于修正偶数情况下偏移量不对称问题
		
		if(middleIndex + offset + fixOffset >= length) return true;
		if(str.charAt(middleIndex + fixOffset + offset) == str.charAt(middleIndex - offset - 1))
			return isPalindrome(str, length, ++offset);
		
		return false;
	}
}
