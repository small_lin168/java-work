package ProblemSet05.Problem01;

public class Main {
	public static void main(String[] args) {
		System.out.println("车牌号码是" + getCode());
	}
	
	public static String getCode() {
		for(int i = 1; i <= 9; i++) {
			for(int j = 1; j <= 9; j++) {
				int code = i * 1100 + j * 11, 
					item = (int) Math.sqrt(code);
				
				if(Math.pow(item, 2) == code && i != j) {
					return Integer.toString(code);
				}
			}
		}
		
		return null;
	}
}
