package ProblemSet05.Problem03;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int len = scan.nextInt();
		int[] hArray = new int[len];
		
		for(int i = 0; i < len; i++)
			hArray[i] = scan.nextInt();
		scan.close();
		
		System.out.println(getMax(hArray, 0, hArray[0]));
	}
	
	public static int getMax(int[] arr, int index, int max) {
		if(index > arr.length - 1) return max;
		
		return getMax(arr, index + 1, arr[index] > max ? arr[index] : max);
	}
}
