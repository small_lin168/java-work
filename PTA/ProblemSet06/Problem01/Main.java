package ProblemSet06.Problem01;

import java.util.Arrays;
import java.util.Scanner;

class PersonSortable implements Comparable<PersonSortable>{
	private String name;
	private int age;
	
	public PersonSortable(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	public String toString() {
		return name + "-" + age;
	}

	public int compareTo(PersonSortable o) {
		int nameCompareResult = this.name.compareTo(o.name);
		if(nameCompareResult == 0)
			return this.age - o.age;
		
		return nameCompareResult;
	}
}

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int len = Integer.parseInt(scan.nextLine());
		PersonSortable[] pList = new PersonSortable[len];
		
		for(int i = 0; i < len; i++) {
			String[] pData = scan.nextLine().split(" ");
			pList[i] = new PersonSortable(pData[0], Integer.parseInt(pData[1]));
		}
		scan.close();
		
		Arrays.sort(pList);
		for(PersonSortable p : pList)
			System.out.println(p);
		
		System.out.println(Arrays.toString(PersonSortable.class.getInterfaces()));
	}
}

