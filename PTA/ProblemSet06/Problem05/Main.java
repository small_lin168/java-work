package ProblemSet06.Problem05;

import java.util.Scanner;

class ArrayUtils {
	public static double findMax(double[] arr, int begin, int end) throws IllegalArgumentException {
		if(begin < 0) throw new IllegalArgumentException("begin:" + begin + " < 0");
		else if(begin >= end) throw new IllegalArgumentException("begin:" + begin + " >= end:" + end);
		else if(end > arr.length) throw new IllegalArgumentException("end:" + end + " > arr.length");
		
		double max = arr[begin];
		for(int i = begin; i < end; i++) {
			if(arr[i] > max) max = arr[i];
		}
		
		return max;
	}
}

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		double[] numArr = new double[n];
		boolean continueLinsten = true;
		
		for(int i = 0; i < n; i++) numArr[i] = scan.nextInt();
		
		while(continueLinsten) {
			try {
				int n1 = scan.nextInt(), n2 = scan.nextInt();
				System.out.println(ArrayUtils.findMax(numArr, n1, n2));
			}
			catch (IllegalArgumentException e) {
				System.out.println(e);
			}
			catch (Exception e) {
				continueLinsten = false;
			}
		}
		scan.close();
		
		try {
		     System.out.println(ArrayUtils.class.getDeclaredMethod("findMax", double[].class,int.class,int.class));
		} catch (Exception e1) {
			
		} 
	}
}
