package ProblemSet06.Problem04;

import java.util.Scanner;
import java.util.Arrays;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		int[] numArr = new int[n];
		
		for(int i = 0; i < n; i++) {
			try {
				numArr[i] = Integer.parseInt(scan.next());
			} catch (Exception e) {
				i--;
				System.out.println(e);
			}
		}
		scan.close();
		
		System.out.println(Arrays.toString(numArr));
	}
}
