package ProblemSet06.Problem06;

import java.util.Scanner;
import java.util.NoSuchElementException;

class IllegalNameException extends RuntimeException {
	private static final long serialVersionUID = 3229322801946529935L;
	
	public IllegalNameException() {
		super();
	}
	
	public IllegalNameException(String msg) {
		super(msg);
	}
	
}

class IllegalScoreException extends Exception{
	private static final long serialVersionUID = 1059038683602760667L;
	
	public IllegalScoreException() {
		super();
	}
	
	public IllegalScoreException(String msg) {
		super(msg);
	}
	
}

class Student {
	private String name;
	private int score;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		if(Character.isDigit(name.charAt(0))) throw new IllegalNameException("the first char of name must not be digit, name=" + name);
			
		this.name = name;
	}
	
	public int getScore() {
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}
	
	public String toString() {
		return "Student [name=" + name + ", score=" + score + "]";
	}
	
	public int addScore(int score) throws IllegalScoreException {
		int tmpScore = this.score + score;
		if(tmpScore < 0 || tmpScore > 100) throw new IllegalScoreException("score out of range, score=" + tmpScore);
		
		this.score = tmpScore;
		return this.score;
	}
	
}

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		while(true) {
			String cmd = scan.nextLine();
			if ("new".equals(cmd)) {
				try {
					String[] sData = scan.nextLine().split(" ");
					if(sData.length != 2) throw new NoSuchElementException();
					
					Student stu = new Student();
					stu.setName(sData[0]);
					stu.addScore(Integer.parseInt(sData[1]));
					System.out.println(stu);
				} catch (Exception e) {
					System.out.println(e);
				}
			} else break;
		}
		scan.close();
		System.out.println("scanner closed");
	}
}
