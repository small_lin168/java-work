package ProblemSet06.Problem02;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

class PersonSortable2 {
	private String name;
	private int age;
	
	public PersonSortable2(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	public String toString() {
		return name + "-" + age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}

class NameComparator implements Comparator<PersonSortable2> {
	
	public int compare(PersonSortable2 o1, PersonSortable2 o2) {
		return o1.getName().compareTo(o2.getName());
	}
	
}

class AgeComparator implements Comparator<PersonSortable2> {

	public int compare(PersonSortable2 o1, PersonSortable2 o2) {
		return o1.getAge() - o2.getAge();
	}
	
}

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int len = Integer.parseInt(scan.nextLine());
		PersonSortable2[] pList = new PersonSortable2[len];
		
		for(int i = 0; i < len; i++) {
			String[] pData = scan.nextLine().split(" ");
			pList[i] = new PersonSortable2(pData[0], Integer.parseInt(pData[1]));
		}
		scan.close();
		
		Arrays.sort(pList, new NameComparator());
		System.out.println("NameComparator:sort");
		for(PersonSortable2 p : pList) System.out.println(p);
		
		Arrays.sort(pList, new AgeComparator());
		System.out.println("AgeComparator:sort");
		for(PersonSortable2 p : pList) System.out.println(p);
		
		System.out.println(Arrays.toString(NameComparator.class.getInterfaces()));
		System.out.println(Arrays.toString(AgeComparator.class.getInterfaces()));
	}
}
