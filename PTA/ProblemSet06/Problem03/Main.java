package ProblemSet06.Problem03;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		int[] arr = new int[5];
		Scanner scan = new Scanner(System.in);
		boolean continueListen = true;
		
		while(continueListen) {
			String[] cmd = scan.nextLine().split(" ");
			try {
				switch(cmd[0]) {
					case "arr":
						arr[Integer.parseInt(cmd[1])] = 0;
						break;
					case "null":
						throw new NullPointerException();
					case "cast":
						@SuppressWarnings("unused")
						Integer tmp = (Integer)((Object) new String(""));
						break;
					case "num":
						Integer.parseInt(cmd[1]);
						break;
					default:
						continueListen = false;
				}
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		
		scan.close();
	}
}
