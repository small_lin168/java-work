## PTA作业代码
### 作业目录
- 实验02
	- [7-1 古埃及探秘-金字塔](./ProblemSet02/Problem01/Main.java)
	- [7-2 jmu-Java-01入门-第一个PAT上Java程序](./ProblemSet02/Problem02/Main.java)
	- [7-3 jmu-Java-01入门-取数字](./ProblemSet02/Problem03/Main.java)
	- [7-4 jmu-Java-01入门-取数字浮点数](./ProblemSet02/Problem04/Main.java)
	- [7-5 jmu-Java-01入门-开根号](./ProblemSet02/Problem05/Main.java)
- 实验03
	- [7-1 校园竞赛-十位评委](./ProblemSet03/Problem01/Main.java)
	- [7-2 jmu-Java-02基本语法-01-综合小测验](./ProblemSet03/Problem02/Main.java)
	- [7-3 jmu-Java-02基本语法-02-StringBuilder](./ProblemSet03/Problem03/Main.java)
	- [7-4 jmu-Java-02基本语法-03-身份证排序](./ProblemSet03/Problem04/Main.java)
- 实验04
	- [7-1 jmu-Java-03面向对象基础-01-构造函数与toString](./ProblemSet04/Problem01/Main.java)
	- [7-2 jmu-Java-03面向对象基础-02-构造函数与初始化块](./ProblemSet04/Problem02/Main.java)
	- [7-3 jmu-Java-03面向对象基础-03-形状](./ProblemSet04/Problem03/Main.java)
	- [7-4 jmu-Java-03面向对象基础-05-覆盖](./ProblemSet04/Problem04/Main.java)
	- [7-5 jmu-Java-03面向对象-06-继承覆盖综合练习-Person、Student、Employee、Company](./ProblemSet04/Problem05/Main.java)
- 实验05
	- [7-1 消失的车](./ProblemSet05/Problem01/Main.java)
	- [7-2 判断回文](./ProblemSet05/Problem02/Main.java)
	- [7-3 我是升旗手](./ProblemSet05/Problem03/Main.java)
- 实验06
	- [7-1 jmu-Java-04面向对象进阶-01-接口-Comparable](./ProblemSet06/Problem01/Main.java)
	- [7-2 jmu-Java-04面向对象进阶--02-接口-Comparator](./ProblemSet06/Problem02/Main.java)
	- [7-3 jmu-Java-06异常-01-常见异常](./ProblemSet06/Problem03/Main.java)
	- [7-4 jmu-Java-06异常-02-使用异常机制处理异常输入](./ProblemSet06/Problem04/Main.java)
	- [7-5 jmu-Java-06异常-03-throw与throws](./ProblemSet06/Problem05/Main.java)
	- [7-6 jmu-Java-06异常-04-自定义异常(综合)](./ProblemSet06/Problem06/Main.java)


- 天梯题集（贡献者：[@small_lin168](https://gitee.com/small_lin168)）
	- [7-2 比较大小](./Tianti/Problem02/Main.java)
	- [7-3 计算指数](./Tianti/Problem03/Main.java)
	- [7-4 计算阶乘和](./Tianti/Problem04/Main.java)
	- [7-5 简单题](./Tianti/Problem05/Main.java)
	- [7-6 跟奥巴马一起画方块](./Tianti/Problem06/Main.java)
	- [7-7 查验身份证](./Tianti/Problem07/Main.java)

### 讨论和建议
如果大家在查看过程中发现错误或不足之处，欢迎大家在[Issues](https://gitee.com/Samler/java-work/issues)中发表自己的见解，也欢迎大家在[Issues](https://gitee.com/Samler/java-work/issues)中进行讨论。

### 代码提交
欢迎大家提交自己的作业代码，PTA作业提交路径命名格式（/ProblemSet实验号/Problem题号-[ID]/Main.java），ID默认从01开始自增，取没有使用过的即可。

提交方法：在上方点击 [Fork](https://gitee.com/Samler/java-work/members#) 按钮后在Fork后的分支仓库后新增自己的路径并编写代码后 [Pull Request](https://gitee.com/Samler/java-work/pull/new) 合并至 **develop** 分支

### 联系方式
QQ：[1045704373](http://sighttp.qq.com/authd?IDKEY=b15913f467b059554c44ebde926c9dd2c7f5927116ba8855) 或 [15181536](http://wpa.qq.com/msgrd?v=3&uin=15181536&site=qq&menu=yes)