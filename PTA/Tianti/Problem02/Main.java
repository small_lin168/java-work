package Tianti.Problem02;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		int N=3;
		int[] sort_result=new int[N];
		for(int i =0;i<N;i++){
			sort_result[i]=sc.nextInt();
		}
		Arrays.sort(sort_result);
		for(int i=0;i<N;i++){
			if(i==N-1){
				System.out.print(sort_result[i]);
				break;
			}
			System.out.print(sort_result[i]+"->");
		}
	}

}
