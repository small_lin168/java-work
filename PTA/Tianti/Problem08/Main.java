package Tianti.Problem08;
 
import java.util.*;
 
public class Pta8 {
	//存储后序遍历结点
    private static Vector<Integer> post = new Vector<>();
    //存储中序遍历结点
    private static Vector<Integer> in = new Vector<>();
    //有序/LinkedHashMap
    private static TreeMap<Integer,Integer> level = new TreeMap<>();
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for(int i = 0; i<n; i++){
        	post.add(sc.nextInt());
        }
        for(int i = 0; i<n; i++){
        	in.add(sc.nextInt());
        }
        pre(n-1,0,n-1,0);
        Set set = level.entrySet();//存储层序遍历结点
        Iterator it = set.iterator();//开始输出层序遍历结点
        Map.Entry entry = (Map.Entry)it.next();
        System.out.print((Integer)entry.getValue());
        while(it.hasNext()){
            entry = (Map.Entry)it.next();
            System.out.print(" "+(Integer)entry.getValue());
        }
    }
 
    private static void pre(int root, int start, int end, int index) {
        if(start > end)return;
        int i = start;
        while(i < end && in.get(i) != post.get(root)){
        	i++;
        }
        level.put(index,post.get(root));//根
        pre(root - 1 - end + i, start, i - 1, 2 * index + 1);//左
        pre(root - 1, i + 1, end, 2 * index + 2);//右
    }
}