package Tianti.Problem09;
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
 
public class Pta9 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String str = reader.readLine();
        //设置一个NN的二维数组
        int arr_i[][] = new int[str.length()][str.length()];
        int res_count =0;
        for(int i=0;i<str.length();i++){
            for(int j=0;j<=i;j++){
                if(i==j){
                	arr_i[j][i] = 1;
                }else if(i-j==1){
                    if(str.charAt(i)==str.charAt(j)){
                    	arr_i[j][i] = 2;
                    }
                }else if(i-j>=2){
                    if(str.charAt(i)==str.charAt(j)&&arr_i[j+1][i-1]>0){
                    	arr_i[j][i] = arr_i[j+1][i-1]+2;
                    }
                }
            }
        }
        for(int i=0;i<arr_i.length;i++){
            for(int j=0;j<arr_i[0].length;j++){
            	res_count = Math.max(res_count, arr_i[i][j]);
            }
        }
        System.out.println(res_count);
    }
}