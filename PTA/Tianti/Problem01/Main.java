package  Tianti.Problem01;
 
import java.util.Scanner;
 
public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		
		//输入完n之后将光标移到一下行
		sc.nextLine();
		int a, b;
		
		String str=sc.nextLine();
//		System.out.println("This is:"+str);
		String[] split = str.split(" ");
		String s = split[0];
		//获取分子
		a = Integer.parseInt(s.substring(0, s.indexOf('/')));
		//获取分母
		b = Integer.parseInt(s.substring(s.indexOf('/') + 1));
		int x, y, common;
		
		common = gcd(a, b);
		a /= common;
		b /= common;
		
		for (int i = 1; i < n; i++) {
			s = split[i];
			x = Integer.parseInt(s.substring(0, s.indexOf('/')));
			y = Integer.parseInt(s.substring(s.indexOf('/') + 1));
			
			common = y * b / gcd(y, b);
			
			a *= common / b;
			x *= common / y;
 
			a += x;
			b = common;
			
			common = gcd(a, b);
			a /= common;
			b /= common;
			
		}
		
		if(a / b != 0) {
			if(a % b != 0) 
				System.out.println(a / b + " " + a % b + "/" + b);
			else
				System.out.println(a / b);
		} else {
			if(a % b != 0) 
				System.out.println(a % b + "/" + b);
			else
				System.out.println(0);
		}
	}
	
	private static int gcd(int a, int b) {
		if(b==0){
			return a;
		}else{
			return gcd(b,a%b);
		}
	}
}