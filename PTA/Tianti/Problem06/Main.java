package Tianti.Problem06;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double n = sc.nextInt(); // 定义N个字符
		String s = sc.next(); 		// 定义字符样式
		if (3 <= n && n <= 21) {
			double h = n / 2; 		// 定义高度
			int x = (int) (n / 2);	//四舍五入
			double y = h - x;
			if (y >= 0.5) {
				h++;
			}
			for (int i = 0; i < (int) h; i++) {  //通过高度来打印
				for (int j = 0; j < n; j++) {	 //通过长度来打印
					System.out.print(s);
				}
				System.out.println();
			}
		}

	}
}
