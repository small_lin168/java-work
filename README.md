# JavaWork
此仓库存放Java的PTA作业和实验作业的源代码供大家参考

### 作业查看入口
- PTA作业：[/PTA](/PTA)
- 实验作业（待完善）：[/EXP](/EXP)

	由于前期未进行作业的整理，因此早前的部分作业代码可能没有放上来，敬请谅解。

### 讨论和建议
如果大家在查看过程中发现错误或不足之处，欢迎大家在[Issues](https://gitee.com/Samler/java-work/issues)中发表自己的见解，也欢迎大家在[Issues](https://gitee.com/Samler/java-work/issues)中进行讨论。

### 联系方式
QQ：[1045704373](http://sighttp.qq.com/authd?IDKEY=b15913f467b059554c44ebde926c9dd2c7f5927116ba8855) 或 [15181536](http://wpa.qq.com/msgrd?v=3&uin=15181536&site=qq&menu=yes)