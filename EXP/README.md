## 实验作业代码
### 作业目录
- 实验06
	- [A Bank Account Class](./Exp06/Problem01)
	- [Representing Names](./Exp06/Problem02)

### 讨论和建议
如果大家在查看过程中发现错误或不足之处，欢迎大家在[Issues](https://gitee.com/Samler/java-work/issues)中发表自己的见解，也欢迎大家在[Issues](https://gitee.com/Samler/java-work/issues)中进行讨论。

### 联系方式
QQ：[1045704373](http://sighttp.qq.com/authd?IDKEY=b15913f467b059554c44ebde926c9dd2c7f5927116ba8855) 或 [15181536](http://wpa.qq.com/msgrd?v=3&uin=15181536&site=qq&menu=yes)