## 实验6作业代码
### 题目详情
- A Bank Account Class
	- [Account.java](./Problem01/Account.java)
	- [ManageAccounts](./Problem01/ManageAccounts.java)
- Representing Names
	- [Name.java](./Problem02/Name.java)
    - [TestNamesName.java](./Problem02/TestNames.java)

### 讨论和建议
如果大家在查看过程中发现错误或不足之处，欢迎大家在[Issues](https://gitee.com/Samler/java-work/issues)中发表自己的见解，也欢迎大家在[Issues](https://gitee.com/Samler/java-work/issues)中进行讨论。

### 代码提交
欢迎大家提交自己的作业代码，实验作业提交路径命名格式（/Exp实验号/Problem题号-[ID]/xxx.java），ID默认从01开始自增，取没有使用过的即可。

提交方法：在上方点击 [Fork](https://gitee.com/Samler/java-work/members#) 按钮后在Fork后的分支仓库后新增自己的路径并编写代码后 [Pull Request](https://gitee.com/Samler/java-work/pull/new) 合并至 **develop** 分支

### 联系方式
QQ：[1045704373](http://sighttp.qq.com/authd?IDKEY=b15913f467b059554c44ebde926c9dd2c7f5927116ba8855) 或 [15181536](http://wpa.qq.com/msgrd?v=3&uin=15181536&site=qq&menu=yes)