package Exp06.Problem02;

public class Name {
	private String first;
	private String middle;
	private String last;
	
	public Name(String first, String middle, String last) {
		this.first = first;
		this.middle = middle;
		this.last = last;
	}
	
	public String getFirst(){
		return this.first;
	}
	
	public String getMiddle(){
		return this.middle;
	}
	
	public String getLast(){
		return this.last;
	}
	
	public String firstMiddleLast(){
		return this.first + " " + this.middle + " " + this.last;
	}
	
	public String lastFirstMiddle(){
		return this.last + ", " + this.first + this.middle;
	}
	
	public boolean equals(Name otherName){
		if(otherName == null)
			return false;
		if(otherName == this)
			return true;
		if(this.strEquals(this.first, otherName.first) && 
				this.strEquals(this.middle, otherName.middle) && 
				this.strEquals(this.last, otherName.last))
			return true;
		
		return false;
	}
	
	public String initials(){
		return this.first.substring(0, 1).toUpperCase() + 
				this.middle.substring(0, 1).toUpperCase() + 
				this.last.substring(0, 1).toUpperCase();
	}
	
	public int length(){
		return this.first.length() + this.middle.length() + this.last.length();
	}
	
	private boolean strEquals(String s1, String s2){
		if(s1 == s2 || s1 != null && s1.equals(s2)) 
			return true;
		
		return false;
	}
}