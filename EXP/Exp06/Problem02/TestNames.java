package Exp06.Problem02;

import java.util.Scanner;

public class TestNames {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String[] n1, n2;
		Name name1, name2;
		System.out.print("Please enter the first name: ");
		n1 = scan.nextLine().split(" ");
		name1 = new Name(n1[0], n1[1], n1[2]);
		
		System.out.print("Please enter the second name: ");
		n2 = scan.nextLine().split(" ");
		name2 = new Name(n2[0], n2[1], n2[2]);
		
		scan.close();
		
		System.out.println(name1.firstMiddleLast());
		System.out.println(name2.firstMiddleLast());
		
		System.out.println(name1.lastFirstMiddle());
		System.out.println(name2.lastFirstMiddle());
		
		System.out.println(name1.initials());
		System.out.println(name2.initials());
		
		System.out.println(name1.length());
		System.out.println(name2.length());
		
		System.out.println("name1 " + (name1.equals(name2) ? "equals" : "not equals") + " name2");
		
	}
}
